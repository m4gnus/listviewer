import React, {PureComponent} from 'react';
import './App.css';

export default class App extends PureComponent{
    constructor(props) {
        super(props);
        // defined urls
        this.proxy = 'https://cors-anywhere.herokuapp.com/';
        this.url = 'https://lumav.net/dummy/categories.json';
        // when this component will be re-rendered
        this.state = {
          content: []
        };
    }

    // Checks that component is mounted and when it's mounted data from given URL is loaded
    componentDidMount(){
        // Fetch is used to load data
        // this.proxy is api what avoids CORS policy
        fetch(this.proxy + this.url)
            .then(response => response.json())
            .then(result => this.setState({content: result.categories}));
    }

    render() {
        const {content} = this.state;
        // Recursive loading starts here
        return (
            <div className="con">
                <Category content={content} />
            </div>
        );
    }
}

// Recursive function to show categories and categories inside categories
const Category = ({content}) => {
    return (
    <ul>
        {content.map(cat => (
            <li key={cat.name}>
                {cat.name}
                {cat.categories.length > 0 && <Category content={cat.categories} />}
            </li>
        ))}
    </ul>)
}
